# test

[![CI Status](https://ci.codeberg.org/api/badges/davidak/test/status.svg)](https://ci.codeberg.org/davidak/test)

We need more testing!

This is a test of [Woodpecker CI](https://woodpecker-ci.github.io/) using the [Nix](https://nixos.org/nix/) package manager to have a reproducible build environment.

[nixpkgs](https://github.com/NixOS/nixpkgs) is [pinned](nix/sources.json) to a specific commit of the latest stable nixos release.

You can see the build log at: https://ci.codeberg.org/davidak/test

## Use build environment

Developers can use the same environment as the CI.

```
nix-shell
```

## Update pinned nixpkgs

This updates the commit in the same nixos release:

```
nix-shell -p niv --run "niv update"
```

To change the nixos release, use:

```
nix-shell -p niv --run "niv update nixpkgs -b nixos-21.11"
```

## License

Copyright (C) 2020 [davidak](https://davidak.de/)

Licensed under the [GNU General Public License v3.0 or later](LICENSE).

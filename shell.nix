{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
}:
pkgs.mkShell {
  name = "dev-shell";
  buildInputs = with pkgs; [
    python3
    python3Packages.black
  ];
}
